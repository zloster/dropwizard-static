package com.gitlab.zloster;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;

import io.dropwizard.testing.ConfigOverride;
import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit.DropwizardAppRule;

import javax.ws.rs.core.Response;

import org.eclipse.jetty.http.HttpStatus;
import org.junit.ClassRule;
import org.junit.Test;

import com.google.common.io.Resources;

public class ServeStaticApplicationIntegrationTest {
	private static final String CONFIG_PATH = ResourceHelpers
			.resourceFilePath("integration-test.yml");
	private static final String STATIC_ASSETS = resourceParentFilePath("wut1.png");

	@ClassRule
	public static final DropwizardAppRule<ServeStaticApplicationConfiguration> RULE = new DropwizardAppRule<>(
			ServeStaticApplication.class, CONFIG_PATH,
			// Change the default assets location
			ConfigOverride.config("assets.overrides./files", STATIC_ASSETS));

	@Test
	public void testHelloWorld() throws Exception {
		final Response reply = RULE.client()
				.target("http://localhost:" + RULE.getLocalPort() + "/files/wut1.png").request()
				.get();
		assertThat(reply.getStatus()).isEqualTo(HttpStatus.OK_200);
	}
	
	/**
     * Detects the absolute parent path of a class path resource.
     *
     * @param resourceClassPathLocation the filename of the class path resource
     * @return the absolute parent path to the denoted resource
     */
    public static String resourceParentFilePath(final String resourceClassPathLocation) {
        try {
            return new File(Resources.getResource(resourceClassPathLocation).toURI()).getParent();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}