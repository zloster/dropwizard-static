package com.gitlab.zloster.resources;

import static org.assertj.core.api.Assertions.assertThat;
import io.dropwizard.views.ViewMessageBodyWriter;
import io.dropwizard.views.ViewRenderExceptionMapper;
import io.dropwizard.views.ViewRenderer;
import io.dropwizard.views.mustache.MustacheViewRenderer;

import java.util.Collections;

import javax.ws.rs.core.Application;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.junit.Test;

import com.codahale.metrics.MetricRegistry;

/**
 * Unit tests for {@link MomentResource}.
 */
public class MomentResourceTest extends JerseyTest {

	@Override
    protected Application configure() {
        forceSet(TestProperties.CONTAINER_PORT, "0");
        ResourceConfig config = new ResourceConfig();
        final ViewRenderer renderer = new MustacheViewRenderer();
        renderer.configure(Collections.singletonMap("fileRoot", "src/main/resources"));
        config.register(new ViewMessageBodyWriter(new MetricRegistry(), Collections.singletonList(renderer)));
        config.register(new ViewRenderExceptionMapper());
        config.register(new MomentResource());
        return config;
}
	
	@Test
	public void getSuccess() {
		final String reply = target("/moment").request().get(String.class);
		assertThat(reply).containsOnlyOnce("Hello from");
		assertThat(reply).containsOnlyOnce("It's about");
		assertThat(reply).containsOnlyOnce(" o'clock.");
		//TODO Verify there are values for the "time" and "year"
	}
}
