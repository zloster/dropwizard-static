package com.gitlab.zloster.resources;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import io.dropwizard.jersey.validation.ValidationErrorMessage;
import io.dropwizard.testing.junit.ResourceTestRule;

import org.glassfish.jersey.test.grizzly.GrizzlyWebTestContainerFactory;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import com.gitlab.zloster.api.CustomObject;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Unit tests for {@link ValidationResource}.
 */
public class ValidationResourceTest {
	@ClassRule
	public static final ResourceTestRule RESOURCE = ResourceTestRule.builder()
			.addResource(new ValidationResource())
			.setTestContainerFactory(new GrizzlyWebTestContainerFactory()).build();
	private CustomObject customObject;

	@Before
	public void setUp() {
		customObject = new CustomObject(2, "abc", "bcd");
	}

	@Test
	public void getSuccess() {
		CustomObject reply = RESOURCE.target("/validate").request().get(CustomObject.class);
		assertThat(reply.getId()).isEqualTo(1l);
		assertThat(reply.getContent()).isEqualTo("default value");
		assertThat(reply.getOtherContent()).isNull();
	}

	@Test
	public void getWithParameterSuccess() {
		CustomObject reply = RESOURCE.target("/validate").queryParam("name", "Laika Kosmicheska")
				.request().get(CustomObject.class);
		assertThat(reply.getId()).isEqualTo(1l);
		assertThat(reply.getContent()).isEqualTo("Laika Kosmicheska");
		assertThat(reply.getOtherContent()).isNull();
	}

	@Test
	public void postValidate() {
		final Response reply = RESOURCE.target("/validate")
				.request(MediaType.APPLICATION_JSON_TYPE)
				.post(Entity.entity(customObject, MediaType.APPLICATION_JSON_TYPE));
		assertThat(reply.getStatus()).isEqualTo(422);
		ValidationErrorMessage msg = reply.readEntity(ValidationErrorMessage.class);
		assertThat(msg.getErrors()).containsOnly("The request body invalid upload");
	}
}
