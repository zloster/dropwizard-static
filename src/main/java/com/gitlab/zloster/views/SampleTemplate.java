package com.gitlab.zloster.views;

import java.time.LocalDateTime;

import io.dropwizard.views.View;

public class SampleTemplate extends View {
	private final LocalDateTime moment;

    public SampleTemplate(LocalDateTime moment) {
    	super("moment.mustache");
        this.moment = moment;
    }

    public LocalDateTime getMoment() {
    	return moment;
    }
}
