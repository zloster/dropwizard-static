package com.gitlab.zloster.api;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CustomValidator implements ConstraintValidator<CustomValidation, CustomObject> {

    @Override
    public void initialize(CustomValidation constraintAnnotation) {
    }

    @Override
    public boolean isValid(CustomObject value, ConstraintValidatorContext context) {
        System.out.println("isValid() CustomValidator called");
        return false;
    }

}