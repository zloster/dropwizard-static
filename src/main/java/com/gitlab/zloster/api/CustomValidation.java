package com.gitlab.zloster.api;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Constraint(validatedBy = {CustomValidator.class})
// Don't remember why it is turned off
//@Target({ElementType.FIELD, ElementType.PARAMETER})
@Target(ElementType.PARAMETER)
@Retention(value = RetentionPolicy.RUNTIME)
public @interface CustomValidation {
    String message() default "invalid upload";

    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}