package com.gitlab.zloster;

import java.util.Map;
import java.util.Set;

import org.apache.http.client.HttpClient;
import org.glassfish.jersey.server.ResourceConfig;

import com.gitlab.zloster.resources.ExternalResource;
import com.gitlab.zloster.resources.MomentResource;
import com.gitlab.zloster.resources.ValidationResource;

import io.dropwizard.Application;
import io.dropwizard.bundles.assets.ConfiguredAssetsBundle;
import io.dropwizard.client.HttpClientBuilder;
import io.dropwizard.jersey.setup.JerseyEnvironment;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.dropwizard.views.ViewBundle;

public class ServeStaticApplication extends Application<ServeStaticApplicationConfiguration> {

    public static void main(final String[] args) throws Exception {
        new ServeStaticApplication().run(args);
    }

    @Override
    public String getName() {
        return "ServeStaticApplication";
    }

    @Override
    public void initialize(final Bootstrap<ServeStaticApplicationConfiguration> bootstrap) {
    	bootstrap.addBundle(new ConfiguredAssetsBundle());
    	bootstrap.addBundle(new ViewBundle<ServeStaticApplicationConfiguration> (){
            @Override
            public Map<String, Map<String, String>> getViewConfiguration(ServeStaticApplicationConfiguration config) {
                return config.getViewRendererConfiguration();
            }
        });
    }

    @Override
    public void run(final ServeStaticApplicationConfiguration configuration,
                    final Environment environment) {
        final ValidationResource validationResource = new ValidationResource();
        final MomentResource momentResource = new MomentResource();
        
        final HttpClient httpClient = new HttpClientBuilder(environment).using(configuration.getHttpClientConfiguration())
                .build(getName());
        final ExternalResource externalResource = new ExternalResource(httpClient);
        
        
        ResourceConfig jrConfig = environment.jersey().getResourceConfig();
        JerseyEnvironment jrEnv = environment.jersey();
        jrConfig.getInstances();
        Set<Object> dwSingletons = jrConfig.getSingletons();
        
        environment.jersey().register(externalResource);
        environment.jersey().register(validationResource);
        environment.jersey().register(momentResource);
    }

}
