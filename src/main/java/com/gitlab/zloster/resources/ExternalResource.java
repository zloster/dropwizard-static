package com.gitlab.zloster.resources;

import java.io.IOException;
import java.util.Optional;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;

import com.codahale.metrics.annotation.Timed;
import com.gitlab.zloster.api.CustomObject;

@Path("/external")
@Produces(MediaType.APPLICATION_JSON)
public class ExternalResource {
	private HttpClient httpClient;
	
	public ExternalResource(HttpClient httpClient) {
		this.httpClient = httpClient;
	}
	
	@GET
    @Timed
    public CustomObject callDirBg() throws ClientProtocolException, IOException {
		HttpGet request = new HttpGet("https://www.dir.bg/");
		request.addHeader("User-Agent", "Apache HTTP client");
		HttpResponse response = httpClient.execute(request);

//		System.out.println("Response Code : " 
//	                + response.getStatusLine().getStatusCode());

        return new CustomObject(response.getStatusLine().getStatusCode(), "Some response received", null);
    }

}
