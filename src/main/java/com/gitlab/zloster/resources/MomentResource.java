package com.gitlab.zloster.resources;

import java.time.LocalDateTime;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Providers;

import org.glassfish.jersey.internal.ExceptionMapperFactory;
import org.glassfish.jersey.spi.ExceptionMappers;

import com.gitlab.zloster.views.SampleTemplate;

@Path("/moment")
@Produces(MediaType.TEXT_HTML)
public class MomentResource {
	@Inject private ExceptionMappers exMapper;
	@Inject private ExceptionMapperFactory exMapperFactory;
	@Inject private Providers jerseyProviders;
	
	@GET
	public SampleTemplate getMoment() {
		return new SampleTemplate(LocalDateTime.now());
	}
}
