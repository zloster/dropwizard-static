# Custom dummy log appender

Added `DummyAppenderFactory` for testing the ability to provide custom implementations for the logging appenders. To test it use the following command line:

```
java -jar target/dropwizard-static-1.0-SNAPSHOT.jar server config1.yml
```

The sample output should be something like this:

```
~/source/dropwizard-static$  java -jar target/dropwizard-static-1.0-SNAPSHOT.jar server config1.yml 
16:44:17,891 |-INFO in ch.qos.logback.classic.LoggerContext[default] - Could NOT find resource [logback-test.xml]
16:44:17,891 |-INFO in ch.qos.logback.classic.LoggerContext[default] - Could NOT find resource [logback.groovy]
16:44:17,891 |-INFO in ch.qos.logback.classic.LoggerContext[default] - Could NOT find resource [logback.xml]
16:44:17,892 |-INFO in ch.qos.logback.classic.BasicConfigurator@5f7da3d3 - Setting up default configuration.
16:44:19,111 |-INFO in ch.qos.logback.classic.jul.LevelChangePropagator@103082dd - Propagating INFO level on Logger[ROOT] onto the JUL framework
16:44:19,112 |-INFO in ch.qos.logback.classic.jul.LevelChangePropagator@103082dd - Propagating DEBUG level on Logger[com.gitlab.zloster] onto the JUL framework
16:44:19,112 |-WARN in ch.qos.logback.core.ConsoleAppender[dummy-appender] - Encoder has not been set. Cannot invoke its init method.
16:44:19,112 |-ERROR in ch.qos.logback.core.ConsoleAppender[dummy-appender] - No encoder set for the appender named "dummy-appender".

127.0.0.1 - - [12/Nov/2019:14:44:28 +0000] "GET /moment HTTP/1.1" 200 125 "-" "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0" 120
127.0.0.1 - - [12/Nov/2019:14:44:36 +0000] "GET /moment HTTP/1.1" 200 125 "-" "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0" 8
```

# GitLab CI and Maven integration

1. Easy continious integration of the project build - Java 8 and Java 11 builds and tests
1. Exporting of the project build artifacts - see the generated project site artifacts in the CI/CD (test coverage, license information and so on)
1. Full access to the normal development toolset - Maven, JDK, JVM
1. Easy new Java version introduction - adding of Java 11 build is implemented as importing a Docker image and changing the Docker image referencies

# Custom bean validation on HTTP Post request

1. Run `mvn clean install` to build
1. Start the application with `java -jar target/dropwizatd-static-1.0-SNAPSHOT.jar server config.yml`
1. Execute `curl -d @validate-post.txt http://localhost:8080/validate --header "Content-Type:application/json"`
1. The response will be:

```
vagrant@tfb-all:~/dropwizard-static$ curl -d @validate http://localhost:8080/validate --header "Content-Type:application/json"
isValid() CustomValidator called - printed in the application console
{"errors":["The request body Invalid upload"]} - printed in the curl console
```

# Serve static files from arbitrary directory

How to start the Serve static files from arbitrary directory application

1. Run `mvn clean install` to build your application
1. (`Linux`) Copy `{project_root}/somewww` directory to `/tmp`
1. Start application with `java -jar target/dropwizard-static-1.0-SNAPSHOT.jar server config.yml`
1. To check that your application is running enter url `http://localhost:8080/files/qt-trayicon-h11606.png` and `http://localhost:8080/files/wut1.png`. You will see some graphics in the browser
1. Request `http://localhost:8080/files/wut.png`. You should see `HTTP ERROR 404`
1. Rename `/tmp/somewww/wut1.png` to `/tmp/somewww/wut.png` and refresh. You should see the file contents in the browser
1. There is caching involved so if you now request again `http://localhost:8080/files/wut1.png` you will still see it even if there isn't such file anymore

# Health Check

Currently not implemented
